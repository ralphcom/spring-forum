package nl.avans.springforum.validation;

import nl.avans.springforum.domain.Thread;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ThreadValidator implements Validator {

    private static final String TITLE = "title";

    @Override
    public boolean supports(Class<?> aClass) {
        return Thread.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Thread thread = (Thread) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, TITLE, "NotEmpty");
        if (thread.getTitle() == null || thread.getTitle().length() < 6 || thread.getTitle().length() > 50) {
            errors.rejectValue(TITLE, "Size.threadForm.title");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "body", "NotEmpty");
        if (thread.getBody() == null || thread.getBody().length() < 5 || thread.getBody().length() > 5000) {
            errors.rejectValue(TITLE, "Size.threadForm.body");
        }
    }
}
