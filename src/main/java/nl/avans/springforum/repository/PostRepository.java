package nl.avans.springforum.repository;

import nl.avans.springforum.domain.Post;

import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post, Long> {
}
