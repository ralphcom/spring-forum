package nl.avans.springforum.repository;

import java.util.Optional;

import nl.avans.springforum.domain.User;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long>{
    Optional<User> findByUsername(String username);
}
