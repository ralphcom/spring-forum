package nl.avans.springforum.repository;

import nl.avans.springforum.domain.Role;

import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findByName(String name);

}
