package nl.avans.springforum.repository;

import java.util.List;

import nl.avans.springforum.domain.Thread;

import org.springframework.data.repository.CrudRepository;

public interface ThreadRepository extends CrudRepository<Thread, Long> {
    List<Thread> findAll();
}
