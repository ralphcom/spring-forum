package nl.avans.springforum.repository;

import nl.avans.springforum.domain.Category;

import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {
}
