package nl.avans.springforum.repository;

import nl.avans.springforum.domain.Tag;

import org.springframework.data.repository.CrudRepository;

public interface TagRepository extends CrudRepository<Tag, Long> {
}
