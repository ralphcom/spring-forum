package nl.avans.springforum.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import nl.avans.springforum.domain.observer.Observer;
import nl.avans.springforum.domain.observer.Subject;
import nl.avans.springforum.domain.state.Concept;
import nl.avans.springforum.domain.state.State;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Post implements Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    private User poster;

    @Setter private String message;

    @OneToMany(targetEntity = User.class)
    private List<Observer> observers;

    @OneToOne
    private State state;

    @Setter private boolean concept;
    @Setter private boolean visible;

    public Post(User poster, String message) {
        this.poster = poster;
        this.message = message;
        this.observers = Collections.singletonList(poster);
        this.state = new Concept(this).createPost();
        this.concept = true;
        this.visible = false;
    }

    //    voorbeeld implementatie, geen idee of dit uiteindelijk zo word
    public String editPost(String newMessage) {
        this.state.editPost(newMessage);
        this.notifyObservers();
        return this.message;
    }

    public void removedPost() {
        this.state.removePost();
        this.notifyObservers();
    }

    public void uploadPost() {
        this.state.uploadPost();
        this.notifyObservers();
    }

    @Override
    public void addObserver(Observer observer) {
        if (this.observers == null) {
            this.observers = new ArrayList<>();
        }
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : this.observers) {
            observer.update();
        }
    }
}
