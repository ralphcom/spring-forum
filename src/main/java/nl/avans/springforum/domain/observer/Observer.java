package nl.avans.springforum.domain.observer;

public interface Observer {
    void update();
}
