package nl.avans.springforum.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import nl.avans.springforum.domain.observer.Observer;
import nl.avans.springforum.domain.observer.Subject;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Thread implements Subject {

    public Thread() {
        this("Enter a title", "And a body", true);
    }

    public Thread(String title, String body, boolean defaultValues) {
        this.title = title;
        this.body = body;
        if (defaultValues) {
            this.category = null;
            this.posters = Collections.emptyList();
            this.tags = Collections.emptyList();
            this.posts = Collections.emptyMap();
            this.observers = Collections.emptyList();
        }
    }

    public Thread(String title, String body, Category category, List<User> posters, List<Tag> tags, Map<Integer, Post> posts) {
        this(title, body, false);
        this.category = category;
        this.posters = posters;
        this.tags = tags;
        this.posts = posts;
    }

    public Thread(String title, String body, Category category, User poster, List<Tag> tags, Post post) {
        this(title, body, false);
        this.category = category;
        this.posters = Collections.singletonList(poster);
        this.tags = tags;
        this.posts.put(0, post);
        this.observers = Collections.singletonList(poster);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotEmpty
    @NotNull
    private String title;

    @NotEmpty
    @NotNull
    private String body;

    @ManyToOne
    private User creator;

    @OneToOne
    private Category category;

    @OneToMany
    private List<User> posters;

    @OneToMany
    private List<Tag> tags;

    @OneToMany(fetch = FetchType.EAGER)
    private Map<Integer, Post> posts;

    @OneToMany(targetEntity = User.class)
    private List<Observer> observers;

    public int addNextPost(Post post) {
        int position = posts.size();
        this.posts.put(position, post);
        this.notifyObservers();
        return position;
    }

    public String getPath() {
        return "/threads/view/" + this.getId();
    }

    @Override
    public void addObserver(Observer observer) {
        if (this.observers == null) {
            this.observers = new ArrayList<>();
        }
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : this.observers) {
            observer.update();
        }
    }
}
