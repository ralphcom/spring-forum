package nl.avans.springforum.domain.state;

import javax.persistence.Entity;

import nl.avans.springforum.domain.Post;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class Removed extends State {
    Removed(final Post post) {
        super(post);
    }

    @Override
    public State editPost(final String message) {
        throw new UnsupportedOperationException("Cannot edit a removed post!");
    }

    @Override
    public State removePost() {
        this.post.setVisible(false);
        return this;
    }
}
