package nl.avans.springforum.domain.state;

import javax.persistence.Entity;

import nl.avans.springforum.domain.Post;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class Posted extends State {
    Posted(final Post post) {
        super(post);
    }

    @Override
    public State uploadPost() {
        throw new UnsupportedOperationException("Cannot post a post twice!");
    }
}
