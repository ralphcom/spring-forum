package nl.avans.springforum.domain.state;

import javax.persistence.Entity;

import nl.avans.springforum.domain.Post;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class Concept extends State {
    public Concept(final Post post) {
        super(post);
    }

    @Override
    public State createPost() {
        this.post.setConcept(true);
        this.post.setVisible(false);
        return this;
    }

    @Override
    public State editPost(final String message) {
        this.post.setMessage(message);
        return this;
    }

    @Override
    public State uploadPost() {
        this.post.setVisible(true);
        this.post.setConcept(false);
        return new Posted(this.post);
    }
}
