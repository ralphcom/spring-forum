package nl.avans.springforum.domain.state;

public enum PostState {
    CONCEPT,
    EDITED,
    POSTED,
    REMOVED
}
