package nl.avans.springforum.domain.state;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import nl.avans.springforum.domain.Post;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public abstract class State {
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    Post post;

    State(final Post post) {
        this.post = post;
    }

    public State createPost() {
        throw new UnsupportedOperationException("Cannot create a post twice!");
    }

    public State uploadPost() {
        throw new UnsupportedOperationException("Cannot upload a post twice!");
    }

    public State editPost(String message) {
        this.post.setMessage(message);
        return new Edited(this.post);
    }
    public State removePost() {
        this.post.setVisible(false);
        return new Removed(this.post);
    }
}
