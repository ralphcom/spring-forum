package nl.avans.springforum.service;

import nl.avans.springforum.domain.User;
import nl.avans.springforum.web.dto.UserDto;

public interface IUserService {
    User save(UserDto accountDto);
    User findByUsername(String username);
}
