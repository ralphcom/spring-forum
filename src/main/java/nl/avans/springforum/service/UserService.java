package nl.avans.springforum.service;

import java.util.Collections;
import java.util.Optional;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;

import nl.avans.springforum.domain.User;
import nl.avans.springforum.repository.RoleRepository;
import nl.avans.springforum.repository.UserRepository;
import nl.avans.springforum.web.dto.UserDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username).get();
    }

    @Transactional
    @Override
    public User save(UserDto accountDto) {
        User user = new User();
        user.setPassword(bCryptPasswordEncoder.encode(accountDto.getPassword()));
        user.setUsername(accountDto.getUsername());
        user.setRoles(Collections.singletonList(roleRepository.findByName("USER_ROLE")));

        return userRepository.save(user);
    }

}
