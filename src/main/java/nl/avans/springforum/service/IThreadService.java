package nl.avans.springforum.service;

import java.util.List;

import nl.avans.springforum.domain.Thread;

public interface IThreadService {
    Thread save(Thread thread);
    List<Thread> findAll();
    Thread findById(long id);
}
