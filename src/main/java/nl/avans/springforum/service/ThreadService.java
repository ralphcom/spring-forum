package nl.avans.springforum.service;

import java.util.List;

import nl.avans.springforum.domain.Thread;
import nl.avans.springforum.exception.NotFoundException;
import nl.avans.springforum.repository.ThreadRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ThreadService implements IThreadService {
    private ThreadRepository threadRepository;

    @Autowired
    public ThreadService(final ThreadRepository threadRepository) {
        this.threadRepository = threadRepository;
    }

    @Override
    public Thread save(Thread thread) {
        return threadRepository.save(thread);
    }

    @Override
    public List<Thread> findAll() {
        return threadRepository.findAll();
    }

    @Override
    public Thread findById(long id) {
        return threadRepository.findById(id).orElseThrow(() -> new NotFoundException("No thread found with id: " + id));
    }
}
