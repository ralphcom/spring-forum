package nl.avans.springforum.web.controller;

import nl.avans.springforum.domain.Thread;
import nl.avans.springforum.repository.ThreadRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {
    private final ThreadRepository threadRepository;

    @Autowired
    public HomeController(ThreadRepository threadRepository) {
        this.threadRepository = threadRepository;
    }

    @GetMapping
    public ModelAndView index() {
        Iterable<Thread> threads = this.threadRepository.findAll();
        return new ModelAndView("threads/list", "threads", threads);
    }
}
