package nl.avans.springforum.web.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import nl.avans.springforum.domain.Thread;
import nl.avans.springforum.domain.User;
import nl.avans.springforum.service.IThreadService;
import nl.avans.springforum.service.IUserService;
import nl.avans.springforum.validation.ThreadValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/threads")
public class ThreadsController {

    private IThreadService threadService;
    private ThreadValidator threadValidator;
    private IUserService userService;

    @Autowired
    public ThreadsController(final IThreadService threadService,
                             final ThreadValidator threadValidatorm,
    final IUserService userService) {
        this.threadService = threadService;
        this.threadValidator = threadValidator;
        this.userService = userService;
    }

    @GetMapping("/create")
    public ModelAndView createForm() {
        Thread thread = new Thread();
        return new ModelAndView("threads/create", "thread", thread);
    }

    @PostMapping("/create")
    @Transactional
    public ModelAndView create(@ModelAttribute("thread") @Valid Thread thread, BindingResult result) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        User user = userService.findByUsername(username);
        thread.setCreator(user);

        if (result.hasErrors()) {
            return new ModelAndView("threads/create", "formErrors", result.getAllErrors());
        }

        thread = this.threadService.save(thread);
        log.debug("Created thread: {}", thread);

        Long threadId = thread.getId();

        return new ModelAndView("redirect:/threads/view/" + threadId);
    }

    @GetMapping("/view/{id}")
    public ModelAndView show(@PathVariable("id") Optional<Thread> thread) {

        if (!thread.isPresent()) {
            return new ModelAndView("error/404");
        }

        Thread thr = thread.get();

        return new ModelAndView("threads/show", "thread", thr);
    }
}
