package nl.avans.springforum.web.controller;

import java.util.Collections;
import java.util.List;

import nl.avans.springforum.domain.Thread;
import nl.avans.springforum.service.ThreadService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("api")
public class ApiController {
    private ThreadService threadService;

    @Autowired
    public ApiController(final ThreadService threadService) {
        this.threadService = threadService;
    }

    @GetMapping("/threads")
    public ResponseEntity<List<Thread>> getAllThreads() {
        List<Thread> threads = Collections.emptyList();
        try {
            threads = threadService.findAll();
        } catch (Exception e) {
            log.error("Exception while retrieving threads: {}", e.getMessage(), e);
            return new ResponseEntity<>(threads, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(threads, HttpStatus.OK);
    }
}
