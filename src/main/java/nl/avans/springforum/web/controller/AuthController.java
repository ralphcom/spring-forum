package nl.avans.springforum.web.controller;

import javax.validation.Valid;

import nl.avans.springforum.domain.User;
import nl.avans.springforum.service.IUserService;
import nl.avans.springforum.validation.UserValidator;
import nl.avans.springforum.web.dto.UserDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/")
public class AuthController {
    private IUserService userService;
    private UserValidator userValidator;

    @Autowired
    public AuthController(final IUserService userService, final UserValidator userValidator) {
        this.userService = userService;
        this.userValidator = userValidator;
    }

    @GetMapping("/login")
    public ModelAndView login() {
        return new ModelAndView("auth/login");
    }

    @GetMapping("/register")
    public ModelAndView register() {
        UserDto userDto = new UserDto();
        return new ModelAndView("auth/register", "user", userDto);
    }

    @Transactional
    @PostMapping("/register")
    public ModelAndView registerUserAccount(
            @ModelAttribute("user") @Valid UserDto userDto,
            BindingResult result, WebRequest request, Errors errors) {
        userValidator.validate(userDto, result);

        if (result.hasErrors()) {
            log.error("Registration failed: {}", result.getAllErrors());
            return new ModelAndView("auth/register", "user", userDto);
        }

        createUserAccount(userDto, result);

        return new ModelAndView("redirect:/login?account_created");
    }

    private User createUserAccount(UserDto accountDto, BindingResult result) {
        User registered;

        try {
            registered = userService.save(accountDto);
        } catch (Exception e) {
            return null;
        }

        return registered;
    }
}
