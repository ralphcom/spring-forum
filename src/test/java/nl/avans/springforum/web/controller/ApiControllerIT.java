package nl.avans.springforum.web.controller;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import nl.avans.springforum.SpringForumApplication;
import nl.avans.springforum.domain.Category;
import nl.avans.springforum.repository.CategoryRepository;
import nl.avans.springforum.repository.ThreadRepository;
import nl.avans.springforum.util.TestDataProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringForumApplication.class, webEnvironment = RANDOM_PORT, value = {
    "spring.profiles.active=test"
})
public class ApiControllerIT {
    @LocalServerPort
    private int localServerPort;

    @Autowired
    private ThreadRepository repository;
    @Autowired
    private CategoryRepository categoryRepository;

    @Before
    public void setUp() {
        RestAssured.port = localServerPort;
        Category category = new Category("testCategory");
        categoryRepository.save(category);
        repository.saveAll(TestDataProvider.generateThreadsWithCategory(category));
    }

    @After
    public void tearDown() {
        repository.deleteAll();
        categoryRepository.deleteAll();
    }

    @Test
    public void getAllThreads() {
        given()
            .contentType(ContentType.JSON)
            .get("/api/threads").peek()
            .then()
            .statusCode(SC_OK)
            .body("$", hasSize(5))
            .body("[0].title", is("ThreadTitle: 0"))
            .body("[0].body", is("ThreadBody: 0"))
            .body("[0].category.name", is("testCategory"))
        ;
    }
}
