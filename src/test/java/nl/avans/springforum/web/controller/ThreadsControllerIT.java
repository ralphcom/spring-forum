package nl.avans.springforum.web.controller;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import nl.avans.springforum.SpringForumApplication;
import nl.avans.springforum.domain.Category;
import nl.avans.springforum.repository.CategoryRepository;
import nl.avans.springforum.repository.ThreadRepository;
import nl.avans.springforum.util.TestDataProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.RestAssured;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringForumApplication.class, webEnvironment = RANDOM_PORT, value = {
    "spring.profiles.active=test"
})
public class ThreadsControllerIT {
    @LocalServerPort
    private int serverPort;

    @Autowired
    private ThreadRepository repository;
    @Autowired
    private CategoryRepository categoryRepository;

    @Before
    public void setUp() {
        RestAssured.port = serverPort;
        Category category = new Category("testCategory");
        categoryRepository.save(category);
        repository.saveAll(TestDataProvider.generateThreadsWithCategory(category));
    }

    @After
    public void tearDown() {
        repository.deleteAll();
        categoryRepository.deleteAll();
    }

    @Test
    public void createThread() {

    }
}
