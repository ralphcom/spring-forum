package nl.avans.springforum.util;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.RunListener;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestRunListener extends RunListener {
    private static final String SUCCESSFUL_LOCATION = "util/tests_successful.txt";
    private static final String FAILED_LOCATION = "util/tests_failed.txt";
    private static final String STARTED_LOCATION = "util/tests_started.txt";

    @Override
    public void testRunStarted(final Description description) throws Exception {
        log.info("\n" + getToPrint(STARTED_LOCATION));
    }

    @Override
    public void testRunFinished(final Result result) throws Exception {
        String toPrint;
        if (result.wasSuccessful()) {
            toPrint = getToPrint(SUCCESSFUL_LOCATION);
        } else {
            toPrint = getToPrint(FAILED_LOCATION);
        }
        log.info("\n" + toPrint);
    }

    private String getToPrint(String fileName) {
        ClassLoader cl = getClass().getClassLoader();

        String toPrint;
        try {
            toPrint = IOUtils.toString(cl.getResourceAsStream(fileName), Charset.forName("UTF-8"));
            return toPrint;
        } catch (IOException e) {
            log.debug(e.getMessage(), e);
            throw new RuntimeException("File {" + fileName + "} not found!");
        }
    }
}
