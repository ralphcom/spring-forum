package nl.avans.springforum.util;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nl.avans.springforum.domain.Category;
import nl.avans.springforum.domain.Post;
import nl.avans.springforum.domain.Tag;
import nl.avans.springforum.domain.Thread;
import nl.avans.springforum.domain.User;

public class TestDataProvider {
    public static List<Thread> generateThreadsWithCategory(Category category) {
        List<Thread> threads = new ArrayList<>();

        List<User> posters = emptyList();
        List<Tag> tags = emptyList();
        Map<Integer, Post> posts = emptyMap();
        Thread thread;
        for (int i = 0; i < 5; i++) {
            thread = new Thread("ThreadTitle: " + i, "ThreadBody: " + i, category, posters, tags, posts);
            threads.add(thread);
        }
        return threads;
    }

    public static List<Thread> generateThreads() {
        return generateThreadsWithCategory(null);
    }
}
