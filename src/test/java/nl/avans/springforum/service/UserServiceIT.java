package nl.avans.springforum.service;

import static org.junit.Assert.assertEquals;

import nl.avans.springforum.SpringForumApplication;
import nl.avans.springforum.domain.Role;
import nl.avans.springforum.domain.User;
import nl.avans.springforum.repository.RoleRepository;
import nl.avans.springforum.repository.UserRepository;
import nl.avans.springforum.web.dto.UserDto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringForumApplication.class, value = {
    "spring.profiles.active=test"
})
public class UserServiceIT {
    private static final String TEST_USER = "test-user";
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    private Role userRole;

    private UserService sut;

    @Before
    public void setUp() {
        sut = new UserService(userRepository, roleRepository, new BCryptPasswordEncoder());
        userRole = new Role();
        userRole.setName("USER_ROLE");
        roleRepository.save(userRole);
    }

    @After
    public void tearDown() {
        userRepository.deleteAll();
        roleRepository.deleteAll();
    }

    @Test
    public void saveUser() {
        UserDto userDto = UserDto.builder()
            .username(TEST_USER)
            .password("TestPassword")
            .matchingPassword("TestPassword")
            .build();

        sut.save(userDto);

        User user = userRepository.findByUsername(TEST_USER).orElseThrow(() -> new UsernameNotFoundException(TEST_USER));

        assertEquals(TEST_USER, user.getUsername());
        assertEquals(1, user.getRoles().size());
        assertEquals("USER_ROLE", user.getRoles().get(0).getName());
//        assertTrue(user.getRoles().contains(userRole));
    }
}
