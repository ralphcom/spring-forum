package nl.avans.springforum.validation;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import nl.avans.springforum.domain.Category;
import nl.avans.springforum.domain.Post;
import nl.avans.springforum.domain.Tag;
import nl.avans.springforum.domain.Thread;
import nl.avans.springforum.domain.User;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

@RunWith(JUnit4.class)
public class ThreadValidatorTest {
    private ThreadValidator sut;

    private Thread thread;
    private Errors errors;

    @Before
    public void setUp() {
        sut = new ThreadValidator();

        Category category = new Category("testCategory");
        List<User> posters = emptyList();
        List<Tag> tags = emptyList();
        Map<Integer, Post> posts = emptyMap();
        thread = new Thread("ThreadTitle", "ThreadBody", category, posters, tags, posts);

        errors = new BeanPropertyBindingResult(thread, "thread");
    }

    @Test
    public void testValid() {
        sut.validate(thread, errors);

        assertFalse(errors.hasErrors());
    }

    @Test
    public void noTitle() {
        thread.setTitle(null);

        sut.validate(thread, errors);
        assertTrue(errors.hasErrors());
        assertEquals(2, errors.getFieldErrorCount());
    }

    @Test
    public void shortTitle() {
        thread.setTitle("12345");

        sut.validate(thread, errors);
        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getFieldErrorCount());
    }

    @Test
    public void longTitle() {
        thread.setTitle("IkWilEenVeelsTeLangeTitleVoorMijnThreadMaarDatMagHelemaalNiet");

        sut.validate(thread, errors);
        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getFieldErrorCount());
    }

    @Test
    public void noBody() {
        thread.setBody(null);

        sut.validate(thread, errors);
        assertTrue(errors.hasErrors());
        assertEquals(2, errors.getFieldErrorCount());
    }

    @Test
    public void shortBody() {
        thread.setBody("1234");

        sut.validate(thread, errors);
        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getFieldErrorCount());
    }
}
