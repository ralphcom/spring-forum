package nl.avans.springforum.validation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import nl.avans.springforum.domain.User;
import nl.avans.springforum.repository.UserRepository;
import nl.avans.springforum.web.dto.UserDto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

@RunWith(MockitoJUnitRunner.class)
public class UserValidatorTest {
    private static final String TEST_USER = "test-user";
    private static final String TEST_PASSWORD = "testPassword";

    UserRepository repository;
    private UserValidator sut;

    private UserDto user;
    private Errors errors;

    @Before
    public void setUp() {
        repository = mock(UserRepository.class);
        when(repository.findByUsername(TEST_USER)).thenReturn(Optional.empty());

        sut = new UserValidator(repository);

        user = UserDto.builder()
            .username(TEST_USER)
            .password(TEST_PASSWORD)
            .matchingPassword(TEST_PASSWORD)
            .build();

        errors = new BeanPropertyBindingResult(user, "user");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testValidate() {
        sut.validate(user, errors);

        assertFalse(errors.hasErrors());
    }

    @Test
    public void testDuplicateUserName() {
        when(repository.findByUsername(TEST_USER)).thenReturn(Optional.of(new User()));

        sut.validate(user, errors);
        assertTrue(errors.hasErrors());
    }

    @Test
    public void testNoUsername() {
        user.setUsername(null);

        sut.validate(user, errors);

        assertTrue(errors.hasErrors());
        assertEquals(2, errors.getFieldErrorCount());
    }

    @Test
    public void testShortUsername() {
        user.setUsername("12");

        sut.validate(user, errors);

        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getFieldErrorCount());
    }

    @Test
    public void testLongUsername() {
        user.setUsername("IkWilEenHeelLangeGebruikersnaamMaarDatMagNiet");

        sut.validate(user, errors);

        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getFieldErrorCount());
    }

    @Test
    public void testNoPassword() {
        user.setPassword(null);

        sut.validate(user, errors);

        assertTrue(errors.hasErrors());
        assertEquals(3, errors.getFieldErrorCount());
    }

    @Test
    public void testShortPassword() {
        user.setPassword("12");
        user.setMatchingPassword("12");

        sut.validate(user, errors);

        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getFieldErrorCount());
    }

    @Test
    public void testLongPassword() {
        user.setPassword("IkWilEenHeelLangWachtwoordMaarDatMagNiet");
        user.setMatchingPassword("IkWilEenHeelLangWachtwoordMaarDatMagNiet");

        sut.validate(user, errors);

        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getFieldErrorCount());
    }

    @Test
    public void testNonMatchingPasswords() {
        user.setMatchingPassword("otherPassword");

        sut.validate(user, errors);

        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getFieldErrorCount());
    }
}
